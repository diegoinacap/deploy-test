﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using WebApplication11.Models;

namespace WebApplication11.Data
{
    public class DbInitializer
    {
        public static void Initialize(ApplicationDbContext context)
        {
            context.Database.EnsureCreated();
            if (context.Pelicula.Any())
            {
                return; // DB has been seeded
            }

            var actores = new Actor[]
            {
                new Actor { Nombre = "Angelina", Apellido = "Jolie",
                    FotoUrl = "https://s3-us-west-2.amazonaws.com/cdn.panda-gossips.com/production/posts/eyecatches/000/003/551/original.jpg?1528625013"
                },
                new Actor {Nombre = "Tom", Apellido = "Cruise",
                    FotoUrl = "https://www.newdvdreleasedates.com/images/profiles/tom-cruise-07.jpg"
                }
            };

            foreach (Actor s in actores)
            {
                context.Actor.Add(s);
            }

            context.SaveChanges();

            var peliculas = new Pelicula[]
            {
                new Pelicula
                {
                    Titulo = "Pelicula Uno", 
                    Genero = "Drama", 
                    Precio = "4000",
                    FechaLanzamiento = DateTime.Parse("2002-07-06") 
                },
                new Pelicula
                {
                    Titulo = "Pelicula Dos",
                    Genero = "Drama",
                    Precio = "4000",
                    FechaLanzamiento = DateTime.Parse("2002-07-06")
                },
                new Pelicula
                {
                    Titulo = "Pelicula Tres",
                    Genero = "Drama",
                    Precio = "4000",
                    FechaLanzamiento = DateTime.Parse("2002-07-06")
                }
            };

            foreach (Pelicula p in peliculas)
            {
                context.Pelicula.Add(p);
            }

            context.SaveChanges();

            var participaciones = new Participacion[]
            {
                new Participacion
                {
                    ActorId = actores.Single(a => a.Apellido == "Cruise").Id,
                    PeliculaId = peliculas.Single(p => p.Titulo == "Pelicula Tres").Id,
                    Rol = Rol.Secundario
                },
                new Participacion
                {
                    ActorId = actores.Single(a => a.Apellido == "Jolie").Id,
                    PeliculaId = peliculas.Single(p => p.Titulo == "Pelicula Tres").Id,
                    Rol = Rol.Principal
                },
                new Participacion
                {
                    ActorId = actores.Single(a => a.Apellido == "Cruise").Id,
                    PeliculaId = peliculas.Single(p => p.Titulo == "Pelicula Uno").Id,
                    Rol = Rol.Principal
                },
                new Participacion
                {
                    ActorId = actores.Single(a => a.Apellido == "Jolie").Id,
                    PeliculaId = peliculas.Single(p => p.Titulo == "Pelicula Dos").Id,
                    Rol = Rol.Principal
                }
            };
            foreach (Participacion p in participaciones)
            {
                context.Participacion.Add(p);
            }

            context.SaveChanges();
        }
    }
}
