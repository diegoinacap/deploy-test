﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using WebApplication11.Models;

namespace WebApplication11.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Pelicula> Pelicula { get; set; }

        public DbSet<Actor> Actor { get; set; }

        public DbSet<Participacion> Participacion { get; set; }

        public DbSet<Etiqueta> Etiqueta { get; set; }

        public DbSet<Comentario> Comentario { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            //builder.Entity<Actor>().ToTable("Actor");
            //builder.Entity<Actor>().ToTable("Etiqueta");
            //builder.Entity<Actor>().ToTable("Pelicula");
            //builder.Entity<Actor>().ToTable("Comentario");

            base.OnModelCreating(builder);
        }
    }
}
