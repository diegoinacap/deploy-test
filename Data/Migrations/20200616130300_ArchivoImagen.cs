﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication11.Data.Migrations
{
    public partial class ArchivoImagen : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "NombreArchivoImagen",
                table: "Actor",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NombreArchivoImagen",
                table: "Actor");
        }
    }
}
