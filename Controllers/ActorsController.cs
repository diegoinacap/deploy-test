﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Xsl;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication11.Data;
using WebApplication11.Models;
using WebApplication11.Models.ViewModels;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace WebApplication11.Controllers
{
    public class ActorsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _hostEnvironment;

        public ActorsController(ApplicationDbContext context, IWebHostEnvironment hostEnvironment)
        {
            _context = context;
            _hostEnvironment = hostEnvironment;
        }

        // GET: Actors
        public async Task<IActionResult> Index()
        {
            return View(await _context.Actor.ToListAsync());
        }

        // GET: Actors/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var actor = await _context.Actor
                .FirstOrDefaultAsync(m => m.Id == id);
            if (actor == null)
            {
                return NotFound();
            }

            return View(actor);
        }

        // GET: Actors/Create
        public IActionResult Create()
        {
            var actor = new Actor();
            actor.Participacions = new List<Participacion>();
            IngresarDatosParticipacionesActor(actor);
            return View();
        }

        // POST: Actors/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nombre,Apellido,FotoUrl,ArchivoImagen")] Actor actor, string[] peliculasSeleccionadas)
        {
            if(peliculasSeleccionadas != null)
            {
                actor.Participacions = new List<Participacion>();
                foreach (var pelicula in peliculasSeleccionadas)
                {
                    var peliculaAgregar = new Participacion { ActorId = actor.Id, PeliculaId = int.Parse(pelicula) };
                    actor.Participacions.Add(peliculaAgregar);
                }
            }
            if (ModelState.IsValid)
            {
                string wwwRootPath = _hostEnvironment.WebRootPath;
                string fileName = Path.GetFileNameWithoutExtension(actor.ArchivoImagen.FileName);
                string extension = Path.GetExtension(actor.ArchivoImagen.FileName);
                actor.NombreArchivoImagen = fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                string path = Path.Combine(wwwRootPath + "/images/", fileName);
                using (var fileStream = new FileStream(path, FileMode.Create))
                {
                    await actor.ArchivoImagen.CopyToAsync(fileStream);
                }
                // insertar registro
                _context.Add(actor);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            IngresarDatosParticipacionesActor(actor);
            return View(actor);
        }

        // GET: Actors/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //var actor = await _context.Actor.FindAsync(id);
            var actor = await _context.Actor
                .Include(a => a.Participacions).ThenInclude(p => p.Pelicula)
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id);
            if (actor == null)
            {
                return NotFound();
            }
            IngresarDatosParticipacionesActor(actor);
            return View(actor);
        }

        private void IngresarDatosParticipacionesActor(Actor actor)
        {
            var peliculas = _context.Pelicula;
            var peliculasActor = new HashSet<int>(actor.Participacions.Select(p => p.PeliculaId));
            var viewModel = new List<ParticipacionData>();

            foreach(var pelicula in peliculas)
            {
                viewModel.Add(new ParticipacionData
                {
                    PeliculaId = pelicula.Id,
                    Titulo = pelicula.Titulo,
                    Asociado = peliculasActor.Contains(pelicula.Id)
                });
            }
            ViewData["Peliculas"] = viewModel;
        }

        // POST: Actors/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int? id, string[] peliculasSeleccionadas)
        {
            if (id == null)
            {
                return NotFound();
            }

            var actorActualizar = await _context.Actor
                .Include(a => a.Participacions).ThenInclude(p => p.Pelicula)
                .FirstOrDefaultAsync(s => s.Id == id);

            if(await TryUpdateModelAsync<Actor>(
                actorActualizar,"",a=>a.Nombre, a=>a.Apellido, a => a.FotoUrl))
            {
                ActualizaParticipacionesActor(peliculasSeleccionadas, actorActualizar);
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    ModelState.AddModelError("", "No fue posible guardar cambios");
                }
                return RedirectToAction(nameof(Index));
            }
            ActualizaParticipacionesActor(peliculasSeleccionadas, actorActualizar);
            IngresarDatosParticipacionesActor(actorActualizar);
            return View(actorActualizar);
        }

        private void ActualizaParticipacionesActor(string[] peliculasSeleccionadas, Actor actorActualizar)
        {
            if (peliculasSeleccionadas == null)
            {
                actorActualizar.Participacions = new List<Participacion>();
                return;
            }

            var peliculasSeleccionadasHS = new HashSet<string>(peliculasSeleccionadas);
            var peliculasActor = new HashSet<int>(actorActualizar.Participacions.Select(p => p.Pelicula.Id));
            foreach (var pelicula in _context.Pelicula)
            {
                if (peliculasSeleccionadasHS.Contains(pelicula.Id.ToString()))
                {
                    if (!peliculasActor.Contains(pelicula.Id))
                    {
                        actorActualizar.Participacions.Add(new Participacion { PeliculaId = pelicula.Id, ActorId = actorActualizar.Id });
                    } else
                    {
                        if (peliculasActor.Contains(pelicula.Id))
                        {
                            Participacion peliculaEliminar = actorActualizar.Participacions.FirstOrDefault(p => p.Id == pelicula.Id);

                            _context.Remove(peliculaEliminar);
                        }
                    }
                }
            }
        }

        // GET: Actors/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var actor = await _context.Actor
                .FirstOrDefaultAsync(m => m.Id == id);
            if (actor == null)
            {
                return NotFound();
            }

            return View(actor);
        }

        // POST: Actors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var actor = await _context.Actor.FindAsync(id);
            _context.Actor.Remove(actor);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ActorExists(int id)
        {
            return _context.Actor.Any(e => e.Id == id);
        }
    }
}
