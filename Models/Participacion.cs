﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication11.Models
{
    public enum Rol
    {
        Principal, Secundario
    }
    public class Participacion
    {
        public int Id { get; set; }
        public int PeliculaId { get; set; }
        public int ActorId { get; set; }
        public Rol? Rol { get; set; }
        public virtual Pelicula Pelicula { get; set; }
        public virtual Actor Actor { get; set; }
    }
}
