﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication11.Models
{
    public class Pelicula
    {
        public int Id { get; set; }
        public string Titulo { get; set; }

        [DataType(DataType.Date)]
        public DateTime FechaLanzamiento { get; set; }
        public string Genero { get; set; }
        public string Precio { get; set; }
        public virtual ICollection<Comentario> Comentarios { get; set; }
        public virtual ICollection<Etiqueta> Etiquetas { get; set; }
        public virtual ICollection<Participacion> Participacions { get; set; }

    }
}
