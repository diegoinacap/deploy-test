﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication11.Models
{
    public class Comentario
    {
        public int Id { get; set; }
        public string Mensaje { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:H:mm:ss zzz dd/MM/yy}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }
    }
}
