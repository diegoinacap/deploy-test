﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication11.Models.ViewModels
{
    public class ParticipacionData
    {
        public int PeliculaId { get; set; }
        public string Titulo { get; set; }
        public bool Asociado { get; set; }
    }
}
