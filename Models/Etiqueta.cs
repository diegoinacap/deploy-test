﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication11.Models
{
    public class Etiqueta
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}
