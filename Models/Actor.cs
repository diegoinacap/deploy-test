﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication11.Models
{
    public class Actor
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string FotoUrl { get; set; }
        public string NombreArchivoImagen { get; set; }

        [NotMapped]
        [DisplayName("Carga Archivo")]
        public IFormFile ArchivoImagen { get; set; }

        public virtual ICollection<Participacion> Participacions { get; set; }
    }
}
